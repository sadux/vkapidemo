﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace VkApiDemo.Enums.SafetyEnums
{
    [Serializable]
    [SuppressMessage("ReSharper", "StaticMemberInGenericType")]
    public abstract class SafetyEnum<TFilter> where TFilter : SafetyEnum<TFilter>, new()
    {
        private string _value;

        protected static TFilter RegisterPossibleValue(string value)
        {
            return new TFilter { _value = value };
        }

        public override string ToString()
        {
            return _value;
        }

        public static bool operator ==(SafetyEnum<TFilter> left, SafetyEnum<TFilter> right)
        {
            if (ReferenceEquals(right, left)) { return true; }
            if (ReferenceEquals(null, left)) { return false; }
            if (ReferenceEquals(null, right)) { return false; }

            return left._value == right._value;
        }

        public static bool operator !=(SafetyEnum<TFilter> left, SafetyEnum<TFilter> right)
        {
            return !(left == right);
        }

        public static TFilter FromJsonString(string response)
        {
            if (string.IsNullOrWhiteSpace(response))
            {
                return null;
            }
            var result = new TFilter { _value = response };
            Activator.CreateInstance(result.GetType());
            return result;
        }

        protected bool Equals(SafetyEnum<TFilter> other)
        {
            return _value == other._value;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) { return false; }
            if (ReferenceEquals(this, obj)) { return true; }
            if (obj.GetType() != GetType()) { return false; }
            return Equals((SafetyEnum<TFilter>)obj);
        }

        public override int GetHashCode()
        {
            return _value.GetHashCode();
        }
    }
}