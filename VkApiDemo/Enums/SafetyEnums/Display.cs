﻿namespace VkApiDemo.Enums.SafetyEnums
{
    public sealed class Display : SafetyEnum<Display>
    {
        public static readonly Display Page = RegisterPossibleValue("page");

        public static readonly Display Popup = RegisterPossibleValue("popup");

        public static readonly Display Wap = RegisterPossibleValue("wap");
    }
}