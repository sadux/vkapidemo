﻿namespace VkApiDemo.Enums.Filters
{
    public sealed class Settings : MultivaluedFilter<Settings>
    {
        public static readonly Settings Notify = RegisterPossibleValue(1 << 0, "notify");

        public static readonly Settings Friends = RegisterPossibleValue(1 << 1, "friends");

        public static readonly Settings Photos = RegisterPossibleValue(1 << 2, "photos");

        public static readonly Settings Audio = RegisterPossibleValue(1 << 3, "audio");

        public static readonly Settings Video = RegisterPossibleValue(1 << 4, "video");

        public static readonly Settings Pages = RegisterPossibleValue(1 << 7, "pages");

        public static readonly Settings AddLinkToLeftMenu = RegisterPossibleValue(1 << 8, "addLinkToLeftMenu");

        public static readonly Settings Status = RegisterPossibleValue(1 << 10, "status");

        public static readonly Settings Notes = RegisterPossibleValue(1 << 11, "notes");

        public static readonly Settings Messages = RegisterPossibleValue(1 << 12, "messages");

        public static readonly Settings Wall = RegisterPossibleValue(1 << 13, "wall");

        public static readonly Settings Ads = RegisterPossibleValue(1 << 15, "ads");

        public static readonly Settings Offline = RegisterPossibleValue(1 << 16, "offline");

        public static readonly Settings Documents = RegisterPossibleValue(1 << 17, "docs");

        public static readonly Settings Groups = RegisterPossibleValue(1 << 18, "groups");

        public static readonly Settings Notifications = RegisterPossibleValue(1 << 19, "notifications");

        public static readonly Settings Statistic = RegisterPossibleValue(1 << 20, "stats");

        public static readonly Settings Email = RegisterPossibleValue(1 << 22, "email");

        public static readonly Settings Market = RegisterPossibleValue(1 << 27, "market");

        public static readonly Settings All = Notify | Friends | Photos | Audio | Video | Pages | Status | Notes | Messages
            | Wall | Ads | Documents | Groups | Notifications | Statistic | Email | Market;
    }
}