﻿using System.Collections.Generic;
using System.Linq;

namespace VkApiDemo.Enums.Filters
{
    public class MultivaluedFilter<TFilter> where TFilter : MultivaluedFilter<TFilter>, new()
    {
        protected MultivaluedFilter()
        {
        }

        protected internal List<string> Selected { get; private set; } = new List<string>();

        public static bool operator ==(MultivaluedFilter<TFilter> left, MultivaluedFilter<TFilter> right)
        {
            if (ReferenceEquals(right, left))
            { return true; }
            if (ReferenceEquals(null, left))
            { return false; }
            if (ReferenceEquals(null, right))
            { return false; }

            return left.Selected.SequenceEqual(right.Selected);
        }

        public static bool operator !=(MultivaluedFilter<TFilter> left, MultivaluedFilter<TFilter> right)
        {
            return !(left == right);
        }

        protected static TFilter RegisterPossibleValue(ulong mask, string value)
        {
            return FromJsonString(value);
        }

        protected static TFilter RegisterPossibleValue(string value)
        {
            return FromJsonString(value);
        }

        public static TFilter FromJsonString(string val)
        {
            var vals = val.Split(',').Select(x => x.Trim());

            var result = new TFilter();

            result.Selected.AddRange(vals.OrderBy(x => x));

            return result;
        }

        public override string ToString()
        {
            return string.Join(",", Selected.ToArray());
        }

        public static TFilter operator |(MultivaluedFilter<TFilter> a, MultivaluedFilter<TFilter> b)
        {
            return new TFilter { Selected = a.Selected.Union(b.Selected).OrderBy(x => x).ToList() };
        }

        protected bool Equals(MultivaluedFilter<TFilter> other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            return obj.GetType() == GetType() && Equals((MultivaluedFilter<TFilter>)obj);
        }

        public override int GetHashCode()
        {
            return Selected?.GetHashCode() ?? 0;
        }
    }
}