﻿using System;
using VkApiDemo.Enums.Filters;

namespace VkApiDemo.Model
{
    public class ApiAuthParams : IApiAuthParams
    {
        public ulong ApplicationId { get; set; }

        public string Login { get; set; }

        public string Password { get; set; }

        public Settings Settings { get; set; }

        public Func<string> TwoFactorAuthorization { get; set; }

        public string AccessToken { get; set; }

        public int TokenExpireTime { get; set; }

        public long UserId { get; set; }

        public long? CaptchaSid { get; set; }

        public string CaptchaKey { get; set; }

        public string Host { get; set; }

        public int? Port { get; set; }

        public string ProxyLogin { get; set; }

        public string ProxyPassword { get; set; }
    }
}