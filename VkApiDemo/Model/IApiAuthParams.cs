﻿using System;
using VkApiDemo.Enums.Filters;

namespace VkApiDemo.Model
{
    public interface IApiAuthParams
    {
        ulong ApplicationId { get; set; }

        string Login { get; set; }

        string Password { get; set; }

        Settings Settings { get; set; }

        Func<string> TwoFactorAuthorization { get; set; }

        string AccessToken { get; set; }

        int TokenExpireTime { get; set; }

        long UserId { get; set; }

        long? CaptchaSid { get; set; }

        string CaptchaKey { get; set; }

        string Host { get; set; }

        int? Port { get; set; }

        string ProxyLogin { get; set; }

        string ProxyPassword { get; set; }
    }
}