﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading;
using VkApiDemo.Exception;
using VkApiDemo.Model;
using VkApiDemo.Utils;

namespace VkApiDemo
{
    public class VkApi
    {
        public DateTimeOffset? LastInvokeTime { get; private set; }

        private IApiAuthParams _ap;

        public delegate void VkApiDelegate(VkApi sender);

        public event VkApiDelegate OnTokenExpires;

        private Timer _expireTimer;

        private readonly object _minIntervalLock = new object();
        private float _minInterval;
        public IBrowser Browser { get; set; }
        private string AccessToken { get; set; }
        public long? UserId { get; set; }

        private float _requestsPerSecond;

        public TimeSpan? LastInvokeTimeSpan
        {
            get
            {
                if (LastInvokeTime.HasValue)
                {
                    return DateTimeOffset.Now - LastInvokeTime.Value;
                }

                return null;
            }
        }

        public float RequestsPerSecond
        {
            get
            {
                return _requestsPerSecond;
            }
            set
            {
                if (value < 0)
                {
                    throw new ArgumentException(@"Value must be positive", nameof(RequestsPerSecond));
                }

                _requestsPerSecond = value;
                if (!(_requestsPerSecond > 0))
                {
                    return;
                }

                lock (_minIntervalLock)
                {
                    _minInterval = (int)(1000 / _requestsPerSecond) + 1;
                }
            }
        }

        public bool IsAuthorized
        {
            get
            {
                return !string.IsNullOrWhiteSpace(AccessToken);
            }
        }

        public VkApi(IServiceCollection serviceCollection = null)
        {
            var container = serviceCollection ?? new ServiceCollection();

            container.RegisterDefaultDependencies();

            IServiceProvider serviceProvider = container.BuildServiceProvider();
            Browser = serviceProvider.GetRequiredService<IBrowser>();
        }

        public string Invoke(string methodName, IDictionary<string, string> parameters, bool skipAuthorization = false)
        {
            if (!skipAuthorization && !IsAuthorized)
            {
                var message = $"Метод '{methodName}' нельзя вызывать без авторизации";
                throw new AccessTokenInvalidException(message);
            }

            var url = "";
            var answer = "";

            Action sendRequest = delegate
            {
                url = $"https://api.vk.com/method/{methodName}";
                LastInvokeTime = DateTimeOffset.Now;
                answer = Browser.GetJson(url, parameters);
            };

            if (RequestsPerSecond > 0 && LastInvokeTime.HasValue)
            {
                if (_expireTimer == null)
                {
                    SetTimer(0);
                }

                lock (_expireTimer)
                {
                    var span = LastInvokeTimeSpan?.TotalMilliseconds;
                    if (span < _minInterval)
                    {
                        var timeout = (int)_minInterval - (int)span;
                    }

                    sendRequest();
                }
            }
            else if (skipAuthorization)
            {
                sendRequest();
            }

            return answer;
        }

        public void Authorize(IApiAuthParams @params)
        {
            if (@params.AccessToken == null)
            {
                AuthorizeWithAntiCaptcha(@params);
                @params.CaptchaSid = null;
                @params.CaptchaKey = "";
            }
            else
            {
                TokenAuth(@params.AccessToken, @params.UserId, @params.TokenExpireTime);
            }

            _ap = @params;
        }

        private void TokenAuth(string accessToken, long? userId, int expireTime)
        {
            if (string.IsNullOrWhiteSpace(accessToken))
            {
                throw new ArgumentNullException(accessToken);
            }

            StopTimer();

            LastInvokeTime = DateTimeOffset.Now;
            SetApiPropertiesAfterAuth(expireTime, accessToken, userId);
            _ap = new ApiAuthParams();
        }

        private void AuthorizeWithAntiCaptcha(IApiAuthParams authParams)
        {
            BaseAuthorize(authParams);
        }

        private void BaseAuthorize(IApiAuthParams authParams)
        {
            StopTimer();

            LastInvokeTime = DateTimeOffset.Now;
            var authorization = Browser.Authorize(authParams);
            if (!authorization.IsAuthorized)
            {
                var message = $"Invalid authorization with {authParams.Login} - {authParams.Password}";
                throw new VkApiAuthorizationException(message, authParams.Login, authParams.Password);
            }
            SetTokenProperties(authorization);
        }

        private void SetTokenProperties(VkAuthorization authorization)
        {
            var expireTime = (Convert.ToInt32(authorization.ExpiresIn) - 10) * 1000;
            SetApiPropertiesAfterAuth(expireTime, authorization.AccessToken, authorization.UserId);
        }

        private void SetApiPropertiesAfterAuth(int expireTime, string accessToken, long? userId)
        {
            SetTimer(expireTime);
            AccessToken = accessToken;
            UserId = userId;
        }

        private void SetTimer(int expireTime)
        {
            _expireTimer = new Timer(
                AlertExpires,
                null,
                expireTime > 0 ? expireTime : Timeout.Infinite,
                Timeout.Infinite
            );
        }

        private void StopTimer()
        {
            _expireTimer?.Dispose();
        }

        private void AlertExpires(object state)
        {
            OnTokenExpires?.Invoke(this);
        }
    }
}