﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace VkApiDemo.Utils
{
    public sealed class WebCallResult
    {
        public Uri RequestUrl { get; private set; }

        public Cookies Cookies { get; }

        public Uri ResponseUrl { get; private set; }

        public string Response { get; private set; }

        public WebCallResult(string url, Cookies cookies)
        {
            RequestUrl = new Uri(url);
            Cookies = cookies;
            Response = string.Empty;
        }

        public void SaveCookies(CookieCollection cookies)
        {
            Cookies.AddFrom(ResponseUrl, cookies);
        }

        public void SaveResponse(Uri responseUrl, Stream stream, Encoding encoding)
        {
            ResponseUrl = responseUrl;

            using (var reader = new StreamReader(stream, encoding))
            {
                Response = reader.ReadToEnd();
            }
        }
    }
}