﻿using System;
using System.Collections.Generic;
using System.Linq;
using VkApiDemo.Exception;

namespace VkApiDemo.Utils
{
    public class VkAuthorization
    {
        private readonly Dictionary<string, string> _nameValues;

        private VkAuthorization(string uriFragment)
        {
            _nameValues = Decode(uriFragment);
        }

        public long UserId
        {
            get
            {
                return GetUserId();
            }
        }

        public string AccessToken
        {
            get
            {
                return GetFieldValue("access_token");
            }
        }

        public int ExpiresIn
        {
            get
            {
                return GetExpiresIn();
            }
        }

        public bool IsAuthorized
        {
            get
            {
                return _nameValues.ContainsKey("access_token");
            }
        }

        public bool IsAuthorizationRequired
        {
            get
            {
                return _nameValues.ContainsKey("__q_hash");
            }
        }

        private long GetUserId()
        {
            var userIdFieldValue = GetFieldValue("user_id");
            long userId;
            if (!long.TryParse(userIdFieldValue, out userId))
            {
                throw new VkApiException("UserId is not long value.");
            }

            return userId;
        }

        private int GetExpiresIn()
        {
            var expiresInValue = GetFieldValue("expires_in");
            int expiresIn;
            if (!int.TryParse(expiresInValue, out expiresIn))
            {
                throw new VkApiException("ExpiresIn is not integer value.");
            }

            return expiresIn;
        }

        private string GetFieldValue(string fieldName)
        {
            if (_nameValues.ContainsKey(fieldName))
            {
                return _nameValues[fieldName];
            }
            else
            {
                throw new KeyNotFoundException(fieldName);
            }
        }

        public static VkAuthorization From(string uriFragment)
        {
            return new VkAuthorization(uriFragment);
        }

        private static Dictionary<string, string> Decode(string urlFragment)
        {
            var uri = new Uri(urlFragment);

            if (string.IsNullOrWhiteSpace(uri.Query) && string.IsNullOrWhiteSpace(uri.Fragment))
            {
                return new Dictionary<string, string>();
            }

            var query = string.IsNullOrWhiteSpace(uri.Query)
                ? uri.Fragment.Substring(1)
                : uri.Query.Substring(1);

            return query.Split(new[] { '&' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => s.Split('='))
                .ToDictionary(s => s[0], s => s[1]);
        }
    }
}