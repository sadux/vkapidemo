﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VkApiDemo.Exception;

namespace VkApiDemo.Utils
{
    internal sealed class WebForm
    {
        public Cookies Cookies { get; }
        private readonly HtmlDocument _html;
        private readonly string _responseBaseUrl;
        private readonly Dictionary<string, string> _inputs;
        private string _lastName;
        public string OriginalUrl { get; }

        private WebForm(WebCallResult result)
        {
            Cookies = result.Cookies;
            OriginalUrl = result.RequestUrl.OriginalString;

            _html = new HtmlDocument();
            _html.LoadHtml(result.Response);

            var uri = result.ResponseUrl;

            _responseBaseUrl = uri.Scheme + "://" + uri.Host + ":" + uri.Port;

            _inputs = ParseInputs();
        }

        public WebForm WithField(string name)
        {
            _lastName = name;

            return this;
        }

        public WebForm And()
        {
            return this;
        }

        public static bool IsOAuthBlank(WebCallResult result)
        {
            var html = new HtmlDocument();
            html.LoadHtml(result.Response);
            var title = html.DocumentNode.SelectSingleNode("//head/title");
            return title.InnerText.ToLowerInvariant() == "oauth blank";
        }

        public WebForm FilledWith(string value)
        {
            if (string.IsNullOrEmpty(_lastName))
            {
                throw new InvalidOperationException("Field name not set!");
            }

            var encodedValue = value;
            if (_inputs.ContainsKey(_lastName))
            {
                _inputs[_lastName] = encodedValue;
            }
            else
            {
                _inputs.Add(_lastName, encodedValue);
            }

            return this;
        }

        private Dictionary<string, string> ParseInputs()
        {
            var inputs = new Dictionary<string, string>();

            var form = GetFormNode();
            foreach (var node in form.SelectNodes("//input"))
            {
                var nameAttribute = node.Attributes["name"];
                var valueAttribute = node.Attributes["value"];

                var name = nameAttribute != null ? nameAttribute.Value : string.Empty;
                var value = valueAttribute != null ? valueAttribute.Value : string.Empty;

                if (string.IsNullOrEmpty(name))
                {
                    continue;
                }

                inputs.Add(name, Uri.EscapeDataString(value));
            }

            return inputs;
        }

        public string ActionUrl
        {
            get
            {
                var formNode = GetFormNode();

                if (formNode.Attributes["action"] == null)
                {
                    return OriginalUrl;
                }

                var link = formNode.Attributes["action"].Value;
                if (!string.IsNullOrEmpty(link) && !link.StartsWith("http", StringComparison.Ordinal)) // относительный URL
                {
                    link = _responseBaseUrl + link;
                }

                return link;
            }
        }

        public static WebForm From(WebCallResult result)
        {
            return new WebForm(result);
        }

        private HtmlNode GetFormNode()
        {
            HtmlNode.ElementsFlags.Remove("form");
            var form = _html.DocumentNode.SelectSingleNode("//form");
            if (form == null)
            {
                throw new VkApiException("Form element not found.");
            }

            return form;
        }

        public byte[] GetRequest()
        {
            return Encoding.UTF8.GetBytes(GetRequestAsStringArray().JoinNonEmpty("&"));
        }

        public IEnumerable<string> GetRequestAsStringArray()
        {
            return _inputs.Select(x => $"{x.Key}={x.Value}");
        }
    }
}