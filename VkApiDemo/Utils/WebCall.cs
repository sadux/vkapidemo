﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using VkApiDemo.Exception;

namespace VkApiDemo.Utils
{
    internal sealed class WebCall : IDisposable
    {
        private readonly HttpClient _request;
        private readonly WebCallResult _result;

        private WebCall(string url, Cookies cookies, IWebProxy webProxy = null, bool allowAutoRedirect = true)
        {
            var baseAddress = new Uri(url);

            var handler = new HttpClientHandler
            {
                CookieContainer = cookies.Container,
                UseCookies = true,
                Proxy = webProxy,
                AllowAutoRedirect = allowAutoRedirect
            };
            _request = new HttpClient(handler)
            {
                BaseAddress = baseAddress,
                DefaultRequestHeaders =
                    {
                        Accept = { MediaTypeWithQualityHeaderValue.Parse("text/html") }
                    }
            };
            _result = new WebCallResult(url, cookies);
        }

        public static WebCallResult MakeCall(string url, IWebProxy webProxy = null)
        {
            using (var call = new WebCall(url, new Cookies(), webProxy))
            {
                var response = call._request.GetAsync(url).Result;
                return call.MakeRequest(response, new Uri(url), webProxy);
            }
        }

        private WebCallResult MakeRequest(HttpResponseMessage response, Uri uri, IWebProxy webProxy)
        {
            using (var stream = response.Content.ReadAsStreamAsync().Result)
            {
                if (stream == null)
                {
                    throw new VkApiException("Response is null.");
                }

                var encoding = Encoding.UTF8;
                _result.SaveResponse(response.RequestMessage.RequestUri, stream, encoding);

                var cookies = _result.Cookies.Container;

                _result.SaveCookies(cookies.GetCookies(uri));
                return response.StatusCode == HttpStatusCode.Redirect
                        ? RedirectTo(response.Headers.Location.AbsoluteUri, webProxy)
                        : _result;
            }
        }

        private WebCallResult RedirectTo(string url, IWebProxy webProxy = null)
        {
            using (var call = new WebCall(url, _result.Cookies, webProxy))
            {
                var headers = call._request.DefaultRequestHeaders;
                headers.Add("Method", "GET");
                headers.Add("ContentType", "text/html");

                var response = call._request.GetAsync(url).Result;

                return call.MakeRequest(response, new Uri(url), webProxy);
            }
        }

        public static WebCallResult Post(WebForm form, IWebProxy webProxy)
        {
            using (var call = new WebCall(form.ActionUrl, form.Cookies, webProxy, false))
            {
                var formRequest = form.GetRequest();

                var headers = call._request.DefaultRequestHeaders;
                headers.Add("Method", "POST");
                headers.Add("ContentType", "application/x-www-form-urlencoded");

                headers.Add("ContentLength", formRequest.Length.ToString());
                headers.Referrer = new Uri(form.OriginalUrl);

                var paramList = new Dictionary<string, string>();
                foreach (var param in form.GetRequestAsStringArray())
                {
                    if (paramList.ContainsKey(param))
                    {
                        continue;
                    }

                    var paramPair = param.Split('=');
                    var key = paramPair[0] + "";
                    var value = paramPair[1] + "";
                    paramList.Add(key, value);
                }

                var request = call._request.PostAsync(form.ActionUrl, new FormUrlEncodedContent(paramList)).Result;
                return call.MakeRequest(request, new Uri(form.ActionUrl), webProxy);
            }
        }

        public static WebCallResult PostCall(string url, IEnumerable<KeyValuePair<string, string>> parameters, IWebProxy webProxy)
        {
            using (var call = new WebCall(url, new Cookies(), webProxy))
            {
                var request = call._request.PostAsync(url, new FormUrlEncodedContent(parameters)).Result;
                return call.MakeRequest(request, new Uri(url), webProxy);
            }
        }

        public void Dispose()
        {
            _request?.Dispose();
        }
    }
}