﻿using System.Collections.Generic;
using VkApiDemo.Model;

namespace VkApiDemo.Utils
{
    public interface IBrowser
    {
        VkAuthorization Authorize(IApiAuthParams authParams);

        string GetJson(string url, IEnumerable<KeyValuePair<string, string>> parameters);
    }
}