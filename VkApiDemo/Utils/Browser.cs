﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using VkApiDemo.Enums.Filters;
using VkApiDemo.Enums.SafetyEnums;
using VkApiDemo.Exception;
using VkApiDemo.Model;

namespace VkApiDemo.Utils
{
    public class Browser : IBrowser
    {
        public IWebProxy Proxy { get; set; }

        private static bool IsAuthSuccessfull(WebCallResult webCallResult)
        {
            return UriHasAccessToken(webCallResult.RequestUrl) || UriHasAccessToken(webCallResult.ResponseUrl);
        }

        private static bool UriHasAccessToken(Uri uri)
        {
            return uri.Fragment.StartsWith("#access_token=", StringComparison.Ordinal);
        }

        public VkAuthorization Authorize(IApiAuthParams authParams)
        {
            var authorizeUrlResult = OpenAuthDialog(authParams.ApplicationId, authParams.Settings);
            if (IsAuthSuccessfull(authorizeUrlResult))
            {
                return EndAuthorize(authorizeUrlResult, Proxy);
            }

            var loginFormPostResult = FilledLoginForm(authParams.Login, authParams.Password, authParams.CaptchaSid, authParams.CaptchaKey, authorizeUrlResult);
            if (IsAuthSuccessfull(loginFormPostResult))
            {
                return EndAuthorize(loginFormPostResult, Proxy);
            }

            if (HasNotTwoFactor(authParams.TwoFactorAuthorization, loginFormPostResult))
            {
                return EndAuthorize(loginFormPostResult, Proxy);
            }

            var twoFactorFormResult = FilledTwoFactorForm(authParams.TwoFactorAuthorization, loginFormPostResult);
            if (IsAuthSuccessfull(twoFactorFormResult))
            {
                return EndAuthorize(twoFactorFormResult, Proxy);
            }

            return EndAuthorize(null, Proxy);
        }

        private WebCallResult FilledTwoFactorForm(Func<string> code, WebCallResult loginFormPostResult)
        {
            var codeForm = WebForm.From(loginFormPostResult)
                .WithField("code")
                .FilledWith(code.Invoke());

            return WebCall.Post(codeForm, Proxy);
        }

        private bool HasNotTwoFactor(Func<string> code, WebCallResult loginFormPostResult)
        {
            return code == null || WebForm.IsOAuthBlank(loginFormPostResult);
        }

        private WebCallResult FilledLoginForm(string email, string password, long? captchaSid, string captchaKey,
            WebCallResult authorizeUrlResult)
        {
            var loginForm = WebForm.From(authorizeUrlResult)
                .WithField("email")
                .FilledWith(email)
                .And()
                .WithField("pass")
                .FilledWith(password);

            if (captchaSid.HasValue)
            {
                loginForm.WithField("captcha_sid")
                    .FilledWith(captchaSid.Value.ToString())
                    .WithField("captcha_key")
                    .FilledWith(captchaKey);
            }

            return WebCall.Post(loginForm, Proxy);
        }

        private WebCallResult OpenAuthDialog(ulong appId, Settings settings)
        {
            var url = CreateAuthorizeUrlFor(appId, settings, Display.Page);
            return WebCall.MakeCall(url, Proxy);
        }

        private static string CreateAuthorizeUrlFor(ulong appId, Settings settings, Display display)
        {
            var builder = new StringBuilder("https://oauth.vk.com/authorize?");

            builder.AppendFormat("client_id={0}&", appId);
            builder.AppendFormat("scope={0}&", settings);
            builder.Append("redirect_uri=https://oauth.vk.com/blank.html&");
            builder.AppendFormat("display={0}&", display);
            builder.Append("response_type=token");

            return builder.ToString();
        }

        private VkAuthorization EndAuthorize(WebCallResult result, IWebProxy webProxy = null)
        {
            if (IsAuthSuccessfull(result))
            {
                var auth = GetTokenUri(result);
                return VkAuthorization.From(auth.ToString());
            }

            if (HasСonfirmationRights(result))
            {
                var authorizationForm = WebForm.From(result);
                var authorizationFormPostResult = WebCall.Post(authorizationForm, webProxy);

                if (!IsAuthSuccessfull(authorizationFormPostResult))
                {
                    throw new VkApiException("URI должен содержать токен!");
                }

                var tokenUri = GetTokenUri(authorizationFormPostResult);
                return VkAuthorization.From(tokenUri.ToString());
            }

            return null;
        }

        private bool HasСonfirmationRights(WebCallResult result)
        {
            var request = VkAuthorization.From(result.RequestUrl.ToString());
            var response = VkAuthorization.From(result.ResponseUrl.ToString());

            return request.IsAuthorizationRequired || response.IsAuthorizationRequired;
        }

        private Uri GetTokenUri(WebCallResult webCallResult)
        {
            if (UriHasAccessToken(webCallResult.RequestUrl))
            {
                return webCallResult.RequestUrl;
            }

            if (UriHasAccessToken(webCallResult.ResponseUrl))
            {
                return webCallResult.ResponseUrl;
            }

            return null;
        }

        public string GetJson(string methodUrl, IEnumerable<KeyValuePair<string, string>> parameters)
        {
            return WebCall.PostCall(methodUrl, parameters, Proxy).Response;
        }
    }
}