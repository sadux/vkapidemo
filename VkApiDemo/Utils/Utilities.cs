﻿using System.Collections.Generic;
using System.Linq;

namespace VkApiDemo.Utils
{
    public static class Utilities
    {
        public static string JoinNonEmpty<T>(this IEnumerable<T> collection, string separator = ",")
        {
            if (collection == null)
            {
                return string.Empty;
            }

            return string.Join(
                    separator,
                    collection.Select(i => i.ToString().Trim())
                    .Where(s => !string.IsNullOrWhiteSpace(s)
                ));
        }
    }
}