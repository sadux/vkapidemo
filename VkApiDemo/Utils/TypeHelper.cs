﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace VkApiDemo.Utils
{
    public static class TypeHelper
    {
        public static void RegisterDefaultDependencies(this IServiceCollection container)
        {
            container.TryAddSingleton<IBrowser, Browser>();
        }
    }
}