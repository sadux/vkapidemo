﻿using System;

namespace VkApiDemo.Exception
{
    [Serializable]
    public class AccessTokenInvalidException : VkApiException
    {
        public AccessTokenInvalidException()
        {
        }

        public AccessTokenInvalidException(string message) : base(message)
        {
        }
    }
}