﻿using System;

namespace VkApiDemo.Exception
{
    [Serializable]
    public class VkApiException : System.Exception
    {
        public VkApiException()
        {
        }

        public VkApiException(string message) : base(message)
        {
        }
    }
}