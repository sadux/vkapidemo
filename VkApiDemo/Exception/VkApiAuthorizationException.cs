﻿using System;

namespace VkApiDemo.Exception
{
    [Serializable]
    public class VkApiAuthorizationException : VkApiException
    {
        public string Email { get; private set; }

        public string Password { get; private set; }

        public VkApiAuthorizationException(string message, string email, string password) : base(message)
        {
            Email = email;
            Password = password;
        }
    }
}